import { call, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { UPDATE_DATA } from '../redux/types';
import { showAlert, successUpdate } from '../redux/actions';

const setUserInLocalStorage = (user) => {
  localStorage.setItem('user', JSON.stringify(user));
};

function* updateData(action) {
  try {
    const data = new FormData();
    data.append('id', action.payload.id);
    data.append('users_name', action.payload.login);
    data.append('users_password', action.payload.password);
    data.append('users_avatar', action.payload.avatar);

    const response = yield call(axios, 'http://f59e6d1a36c3.ngrok.io/users/update', {
      method: 'post',
      headers: {
        'content-type': `multipart/form-data;`,
      },
      data,
    });
    const payload = response.data;
    yield setUserInLocalStorage(payload);
    yield put(successUpdate(payload));
  } catch (error) {
    yield put(showAlert('Что-то пошло не так'));
  }
}

export default function* updateDataSaga() {
  yield takeEvery(UPDATE_DATA, updateData);
}
