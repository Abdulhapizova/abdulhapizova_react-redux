import { all } from 'redux-saga/effects';
import getNewsSaga from './getNews';
import addUserSaga from './addUser';
import addNewsSaga from './addNews';
import findUserSaga from './findUser';
import findNewsSaga from './findNews';
import updateDataSaga from './updateData';
import findOtherSaga from './findOther';

export default function* rootSaga() {
  yield all([
    getNewsSaga(),
    addUserSaga(),
    addNewsSaga(),
    findUserSaga(),
    findNewsSaga(),
    updateDataSaga(),
    findOtherSaga(),
  ]);
}
