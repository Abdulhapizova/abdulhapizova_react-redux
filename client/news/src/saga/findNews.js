import { call, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { FIND_NEWS_USER } from '../redux/types';
import { successfulFindNews } from '../redux/actions';

const getUserFromLocalStorage = () => JSON.parse(localStorage.getItem('user')) || [];

function* findNews() {
  try {
    const findUser = {
      token: getUserFromLocalStorage().users_token,
    };
    const response = yield call(axios, 'http://f59e6d1a36c3.ngrok.io/news/', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      data: JSON.stringify(findUser),
    });
    const payload = response.data;
    yield put(successfulFindNews(payload));
  } catch (error) {
    yield put(successfulFindNews([]));
  }
}

export default function* findNewsSaga() {
  yield takeEvery(FIND_NEWS_USER, findNews);
}
