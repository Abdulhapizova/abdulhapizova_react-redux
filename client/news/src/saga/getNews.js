import { call, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { REQUEST_POSTS } from '../redux/types';
import { showAlert, requestNews } from '../redux/actions';

function* getNews() {
  try {
    const response = yield call(axios, 'http://f59e6d1a36c3.ngrok.io/news/', {
      method: 'get',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
    });
    const payload = response.data;
    yield put(requestNews(payload));
  } catch (error) {
    yield put(showAlert('Что-то пошло не так'));
  }
}

export default function* getNewsSaga() {
  yield takeEvery(REQUEST_POSTS, getNews);
}
