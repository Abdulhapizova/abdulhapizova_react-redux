import { call, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { FIND_OTHER_USER } from '../redux/types';
import { showAlert, successFindOther } from '../redux/actions';

function* findOthers(action) {
  try {
    const findUser = {
      name: action.payload,
    };
    const response = yield call(axios, `http://f59e6d1a36c3.ngrok.io/news/:id`, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      data: JSON.stringify(findUser),
    });
    const payload = response.data;
    yield put(successFindOther(payload));
  } catch (error) {
    yield put(showAlert('Что-то пошло не так'));
  }
}

export default function* findOtherSaga() {
  yield takeEvery(FIND_OTHER_USER, findOthers);
}
