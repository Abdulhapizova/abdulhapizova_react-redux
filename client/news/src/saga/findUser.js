import { call, takeEvery, put } from 'redux-saga/effects';
import { FIND_USER } from '../redux/types';
import { showAlert, successFindUser } from '../redux/actions';
import axiosUsers from '../utils/axios';

export function* findUser(action) {
  try {
    const oldUsers = {
      users_password: action.payload.password,
      users_name: action.payload.name,
    };
    const response = yield call(axiosUsers, oldUsers);
    yield put(successFindUser(response));
  } catch (error) {
    yield put(showAlert('Что-то пошло не так'));
  }
}

export default function* findUserSaga() {
  yield takeEvery(FIND_USER, findUser);
}
