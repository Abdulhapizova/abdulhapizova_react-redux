import { call, takeEvery, put } from 'redux-saga/effects';
import axios from 'axios';
import { ADD_USERS } from '../redux/types';
import { showAlert, successAddUser } from '../redux/actions';

const setUserInLocalStorage = (user) => {
  localStorage.setItem('user', JSON.stringify(user));
};

function* addUser(action) {
  try {
    const data = new FormData();

    data.append('users_name', action.payload.data.login);
    data.append('users_password', action.payload.data.password);
    data.append('users_avatar', action.payload.avatar);

    const response = yield call(axios, 'http://f59e6d1a36c3.ngrok.io/users/signup/', {
      method: 'post',
      headers: {
        'content-type': `multipart/form-data;`,
      },
      data,
    });
    const payload = response.data;
    yield setUserInLocalStorage(payload);
    yield put(successAddUser(payload));
  } catch (error) {
    yield put(showAlert('Что-то пошло не так'));
  }
}

export default function* addUserSaga() {
  yield takeEvery(ADD_USERS, addUser);
}
