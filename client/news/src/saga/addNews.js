import { call, takeEvery, put } from 'redux-saga/effects';
import axios from 'axios';
import { ADD_NEWS } from '../redux/types';
import { showAlert, successAddNews } from '../redux/actions';

const getUserFromLocalStorage = () => JSON.parse(localStorage.getItem('user')) || [];

function* addNew(action) {
  try {
    const data = new FormData();
    data.append('title', action.payload.title);
    data.append('author', action.payload.author);
    data.append('description', action.payload.description);
    data.append('tag', action.payload.tag);
    data.append('token', getUserFromLocalStorage().users_token);
    data.append('picture', action.payload.picture);

    const response = yield call(axios, 'http://f59e6d1a36c3.ngrok.io/news/uploads/photo', {
      method: 'post',
      headers: {
        'content-type': `multipart/form-data;`,
      },
      data,
    });
    const payload = response.data;
    yield put(successAddNews(payload));
  } catch (error) {
    yield put(showAlert('Что-то пошло не так'));
  }
}

export default function* addNewsSaga() {
  yield takeEvery(ADD_NEWS, addNew);
}
