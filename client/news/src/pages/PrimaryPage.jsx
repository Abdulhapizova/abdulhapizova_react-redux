import React, { useCallback } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import Login from './Login';
import NewsList from '../components/NewsList';
import FindNews from '../components/FindNews';
import Pagination from '../components/Pagination';
import { paginationNews, pagination, putNews, findOldUser, findNewsUser } from '../redux/actions';

const PAGE_SIZE = 5;

const PrimaryPage = ({ news, filterNews, putNews }) => {
  let newArray = [];
  let renderArray = [];

  if (!news) {
    putNews();
  }

  if (!filterNews.length) {
    newArray = _.chunk(news, PAGE_SIZE);
    [renderArray] = newArray;
  } else {
    newArray = _.chunk(filterNews, PAGE_SIZE);
    [renderArray] = newArray;
  }

  const pageChangeHandler = useCallback(
    (selectPage) => {
      pagination(selectPage.selected);
      paginationNews(newArray[selectPage.selected]);
    },
    [newArray],
  );

  return (
    <div className="container center">
      <Login />
      <FindNews />
      <NewsList filterArray={renderArray} />
      <Pagination className="pagination" pageChangeHandler={pageChangeHandler} page={newArray.length} />
    </div>
  );
};

const mapStateToProps = (state) => ({
  news: state.news.value,
  filterNews: state.filterNews.value,
});

const mapDispatchToProps = {
  findOldUser,
  findNewsUser,
  putNews,
};

PrimaryPage.propTypes = {
  news: PropTypes.arrayOf(PropTypes.objectOf()).isRequired,
  filterNews: PropTypes.arrayOf(PropTypes.objectOf()),
  putNews: PropTypes.func.isRequired,
};

PrimaryPage.defaultProps = {
  filterNews: [],
};

export default connect(mapStateToProps, mapDispatchToProps)(PrimaryPage);
