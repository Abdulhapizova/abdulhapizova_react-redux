import React, { useEffect, useState } from 'react';
import PersonalCabinet from './PersonalCabinet';
import PrimaryPage from './PrimaryPage';

const getUserFromLocalStorage = () => JSON.parse(localStorage.getItem('user')) || [];

const Enter = () => {
  const [user, setUser] = useState([]);

  useEffect(() => {
    setUser(getUserFromLocalStorage());
  }, [])

  return (
    <>
      {user.users_name ? (
        <PersonalCabinet name={user.users_name} id={user.id} avatar={user.users_avatar} />
      ) : (
        <PrimaryPage />
      )}
    </>
  );
};

export default Enter;
