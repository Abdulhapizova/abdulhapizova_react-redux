import React from 'react';
import { useParams } from 'react-router-dom';
import CabinetOtherUser from '../components/CabinetOtherUser';

const EnterToOtherUser = () => {
  const { id } = useParams();

  return (
    <div className="container position-top">
      {id ? (
        <p>
          Вы находитесь на странице пользователя
          {id}
          <CabinetOtherUser name={id} />
        </p>
      ) : (
        <p>Пользователь не найден</p>
      )}
    </div>
  );
};

export default EnterToOtherUser;
