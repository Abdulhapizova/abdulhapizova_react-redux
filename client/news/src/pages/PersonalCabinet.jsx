import React, { useCallback, useState, useRef } from 'react';
import { Link } from 'react-router-dom';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { findNewsUser, paginationNews, pagination, updatePersonalData } from '../redux/actions';
import ModalForm from '../components/ModalForm';
import ModalUpdateProfile from '../components/ModalUpdateProfile';
import NewsList from '../components/NewsList';
import Pagination from '../components/Pagination';
import Button from '../components/Button';
import FormFindNewsUser from '../components/FormFindNewsUser';

const getUserFromLocalStorage = () => JSON.parse(localStorage.getItem('user')) || [];
const PAGE_SIZE = 5;
const INITIAL_STATE = {
  login: '',
  password: '',
};

const PersonalCabinet = ({ news = [], findNewsUser, paginationNews, pagination, updatePersonalData }) => {
  const [currUser, setCurrUser] = useState(INITIAL_STATE);
  const avatarRef = useRef(null);
  const authorRef = useRef(null);
  let newArray = [];
  let pages;

  useState(() => {
    setCurrUser(getUserFromLocalStorage());
    authorRef.current = getUserFromLocalStorage().users_name;
    avatarRef.current = getUserFromLocalStorage().users_avatar;
  }, []);

  const handleChangeInputLogin = useCallback((event) => {
    if (event.target.value.trim()) {
      setCurrUser((prev) => ({ ...prev, login: event.target.value.trim() }));
    }
  }, []);

  const handleChangeInputPassword = useCallback((event) => {
    if (event.target.value.trim()) {
      setCurrUser((prev) => ({ ...prev, password: event.target.value.trim() }));
    }
  }, []);

  const handleChangeInputAvatar = useCallback((event) => {
    [avatarRef.current] = event.target.files;
  }, []);

  const handleSubmit = () => {
    updatePersonalData(currUser.id, currUser.users_name, currUser.users_password, avatarRef.current);
  };

  const handlerSubmitExit = useCallback(() => {
    localStorage.clear();
  }, []);

  if (news.length) {
    newArray = _.chunk(news, PAGE_SIZE);
    pages = newArray.length;
  } else {
    findNewsUser(currUser.users_name, currUser.id);
    pages = 0;
  }

  const pageChangeHandler = useCallback(
    (selectPage) => {
      pagination(selectPage.selected);
      paginationNews(newArray[selectPage.selected]);
    },
    [newArray, pagination, paginationNews],
  );

  return (
    <div className="box-container">
      <div className="container">
        <p className="title top">
          Вы вошли как:
          <br />
          {currUser.users_name}
        </p>
        <Link to="/general">
          <Button type="button" className="btn btn-info" title="Перейти на главную страницу" />
        </Link>
        <br />
        <img
          className="avatar"
          src={`http://f59e6d1a36c3.ngrok.io/uploads/profile/${currUser.users_name}`}
          alt="avatar"
        />
        <div className="button-group">
          <ModalForm id="formElement" enctype="multipart/form-data" author={authorRef.current} />
          <ModalUpdateProfile
            id="formElementUpdate"
            enctype="multipart/form-data"
            handleChangeInputLogin={handleChangeInputLogin}
            handleChangeInputPassword={handleChangeInputPassword}
            handleChangeInputAvatar={handleChangeInputAvatar}
            handleSubmit={handleSubmit}
          />
          <Link to="/">
            <Button className="btn btn-dark" type="button" onClick={handlerSubmitExit} title="Выйти" />
          </Link>
        </div>
        <NewsList />
        <Pagination className="pagination" pageChangeHandler={pageChangeHandler} page={pages} />
      </div>
      <FormFindNewsUser />
    </div>
  );
};

const mapStateToProps = (state) => ({
  news: state.news.value,
  newUser: state.addUser.addUsers,
  realUser: state.findUser,
});

const mapDispatchToProps = {
  findNewsUser,
  pagination,
  paginationNews,
  updatePersonalData,
};

PersonalCabinet.propTypes = {
  news: PropTypes.objectOf(),
  findNewsUser: PropTypes.func.isRequired,
  updatePersonalData: PropTypes.func.isRequired,
  paginationNews: PropTypes.func.isRequired,
  pagination: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalCabinet);
