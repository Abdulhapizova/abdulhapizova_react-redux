import React, { useCallback, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { findNewsUser } from '../redux/actions';
import Button from '../components/Button';

const getUserFromLocalStorage = () => JSON.parse(localStorage.getItem('user')) || [];

const SuccessRegistration = () => {
  const [user, setUser] = useState({});

  useEffect(() => {
    setUser(getUserFromLocalStorage());
  }, []);

  const handlerInput = useCallback(() => {
    findNewsUser(user.users_token);
  }, [user.users_token]);

  return (
    <>
      {user.users_name ? (
        <>
          <p>
            Поздравляем,
            {user.users_name}
            ,регистрация прошла успешно!
          </p>
          <Link className="btn btn-dark" to={`user/${user.id}`}>
            <Button onClick={handlerInput} type="button" className="btn btn-dark" title="Перейти в личный кабинет" />
          </Link>
        </>
      ) : null}
    </>
  );
};

const mapStateToProps = (state) => ({
  newUser: state.addUser.addUsers,
});

const mapDispatchToProps = {
  findNewsUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(SuccessRegistration);
