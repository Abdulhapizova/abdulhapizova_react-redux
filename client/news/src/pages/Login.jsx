import React, { useCallback, useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { findOldUser, successFindUser, findNewsUser } from '../redux/actions';
import Input from '../components/Input';
import Button from '../components/Button';

const Login = () => {
  const [name, setNames] = useState('');
  const [password, setPassword] = useState('');

  const handlerInputChangeName = useCallback((event) => {
    setNames(event.target.value);
  }, []);

  const handlerInputChangePassword = useCallback((event) => {
    setPassword(event.target.value);
  }, []);

  const handlerSubmitForms = () => {
    findOldUser(password, name);
  };

  return (
    <div className="login">
      <form className="form-inline" id="accept-form" onSubmit={() => handlerSubmitForms()}>
        <Input
          className="form-control"
          type="text"
          onChange={handlerInputChangeName}
          placeholder="Введите Ваш логин/e-mail"
          value={name}
        />
        <Input
          className="form-control"
          type="password"
          onChange={handlerInputChangePassword}
          placeholder="Введите Ваш пароль"
          value={password}
        />
        <Button className="btn btn-primary" id="accept-button" type="submit" title="Вход" />
      </form>
      <Link className="btn btn-primary" to="registration">
        Регистрация
      </Link>
    </div>
  );
};

const mapStateToProps = (state) => ({
  realUser: state.findUser,
});

const mapDispatchToProps = {
  findOldUser,
  successFindUser,
  findNewsUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
