import React, { useCallback, useState, useRef } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNewUser } from '../redux/actions';
import SuccessRegistration from './SuccessRegistration';
import Alert from '../components/Alert';
import Button from '../components/Button';
import Input from '../components/Input';

const getUserFromLocalStorage = () => JSON.parse(localStorage.getItem('user')) || [];
const initialState = { login: '', password: '' };

const Registration = ({ addNewUser }) => {
  const [state, setState] = useState(initialState);
  const avatarRef = useRef();
  const nameUser = getUserFromLocalStorage().users_name;

  const changeInputHandlerLogin = useCallback(
    (event) => {
      if (event.target.value.trim()) {
        setState({ ...state, login: event.target.value.trim() });
      }
    },
    [state],
  );

  const changeInputHandlerPassword = useCallback(
    (event) => {
      if (event.target.value.trim()) {
        setState({ ...state, password: event.target.value.trim() });
      }
    },
    [state],
  );

  const handleChangeLogo = useCallback((event) => {
    [avatarRef.current] = event.target.files;
  }, []);

  const handlerSubmitUsers = (event) => {
    event.preventDefault();
    addNewUser(state, avatarRef.current);
  };

  return (
    <div className="box-container">
      <form onSubmit={handlerSubmitUsers} encType="multipart/form-data">
        <p>
          Чтобы иметь возможность добавлять и изменять свои новости, пожалуйста, зарегистрируйтесь.
          <br />
          Необходимо заполнить все поля. <br /> <br />
          Регистрация может занять немного времени, пожалуйста, подождите.
        </p>
        <Input
          required
          className="form-control"
          data-testid="login-input"
          type="text"
          id="login"
          onChange={changeInputHandlerLogin}
          placeholder="Введите логин/e-mail"
        />
        <Input
          required
          className="form-control"
          data-testid="password-input"
          type="password"
          id="password"
          onChange={changeInputHandlerPassword}
          placeholder="Введите пароль"
        />
        <br />
        <p>Загрузить аватар:</p>
        <Input required type="file" accept="image/*" onChange={handleChangeLogo} name="users_avatar" />
        <Button className="btn btn-light margin-button" type="submit" title="Регистрация" />
        <Link className="btn btn-light" to="/">
          Назад
        </Link>
      </form>
      {nameUser ? <SuccessRegistration /> : <Alert />}
    </div>
  );
};

Registration.propTypes = {
  addNewUser: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  newUser: state.addUser,
});

const mapDispatchToProps = {
  addNewUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(Registration);
