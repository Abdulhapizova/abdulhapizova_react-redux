import React, { useCallback, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as _ from 'lodash';
import { Link } from 'react-router-dom';
import NewsList from '../components/NewsList';
import FindNews from '../components/FindNews';
import Pagination from '../components/Pagination';
import { paginationNews, pagination, putNews } from '../redux/actions';

const getUserFromLocalStorage = () => JSON.parse(localStorage.getItem('user')) || [];
const PAGE_SIZE = 5;

const GeneralPage = ({ news = [], filterNews = [], findNewsUser }) => {
  const [user, setUser] = useState({});
  let newArray = [];
  let renderArray = [];

  useEffect(() => {
    setUser(getUserFromLocalStorage());
  }, []);

  if (!news) {
    putNews();
  }

  if (!filterNews.length) {
    newArray = _.chunk(news, PAGE_SIZE);
    [renderArray] = newArray;
  } else {
    newArray = _.chunk(filterNews, PAGE_SIZE);
    [renderArray] = newArray;
  }

  const pageChangeHandler = useCallback(
    (selectPage) => {
      pagination(selectPage.selected);
      paginationNews(newArray[selectPage.selected]);
    },
    [newArray],
  );

  const findNews = () => {
    findNewsUser(user.id, user.users_name);
  };

  return (
    <>
      {news.length === 0 ? (
        <p> Wait </p>
      ) : (
        <>
          <p className="title top">
            Вы вошли как:
            {user.users_name}
          </p>
          <Link to={`user/${user.id}`}>
            <button type="button" className="btn btn-info" onClick={findNews}>
              Вернуться в личный кабинет
            </button>
          </Link>
          <FindNews />
          <NewsList filterArray={renderArray} />
          <Pagination className="pagination" pageChangeHandler={pageChangeHandler} page={newArray.length} />
        </>
      )}
    </>
  );
};

GeneralPage.propTypes = {
  news: PropTypes.arrayOf(PropTypes.objectOf),
  filterNews: PropTypes.arrayOf(PropTypes.objectOf).isRequired,
  findNewsUser: PropTypes.arrayOf(PropTypes.objectOf).isRequired,
};

const mapStateToProps = (state) => ({
  news: state.news.value,
  filterNews: state.filterNews.value,
});

export default connect(mapStateToProps)(GeneralPage);
