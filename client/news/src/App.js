import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Enter from './pages/Enter';
import Login from './pages/Login';
import Registration from './pages/Registration';
import PersonalCabinet from './pages/PersonalCabinet';
import GeneralPage from './pages/GeneralPage';
import SuccessRegistration from './pages/SuccessRegistration';
import EnterToOtherUsers from './pages/EnterToOtherUsers';
import NotFound from './pages/NotFound';

import './App.css';

const App = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Enter} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/registration" component={Registration} />
      <Route exact path="/general" component={GeneralPage} />
      <Route exact path="/success" component={SuccessRegistration} />
      <Route exact path="/enter/:id" component={EnterToOtherUsers} />
      <Route exact path="/user/:id" component={PersonalCabinet} />
      <Route component={NotFound} />
    </Switch>
  </Router>
);

export default App;
