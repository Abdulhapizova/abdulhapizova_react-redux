import React, { useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { findAll, findTags, findAuthors, putNews, resultNews } from '../redux/actions';

import Select from './Select';
import Button from './Button';
import Input from './Input';

const FindNews = ({ news, filter }) => {
  const [state, setState] = useState('');
  const options = [{ title: 'all' }, { title: 'tags' }, { title: 'authors' }];

  const changeFilter = (event) => {
    const action = event.target.value;
    switch (action) {
      case 'all':
        findAll(action);
        break;
      case 'tags':
        findTags(action);
        break;
      case 'authors':
        findAuthors(action);
        break;
      default:
        return findAll();
    }
    return filter;
  };

  const filterNews = (find) => {
    let resultOfFind = [];
    switch (filter) {
      case 'tags':
        news.forEach((item) => {
          if (item.news_tag.indexOf(find) !== -1) {
            resultOfFind.push(item);
          }
        });
        break;
      case 'authors':
        news.forEach((item) => {
          if (item.news_author.indexOf(find) !== -1) {
            resultOfFind.push(item);
          }
        });
        break;
      default:
        news.forEach((item) => {
          const keys = Object.keys(item);
          if (String(item[keys]).indexOf(find) !== -1) {
            resultOfFind.push(item);
          }
        });
    }
    if (!find) {
      resultOfFind = news;
    }
    return resultOfFind;
  };

  const changeInputHandler = (event) => {
    event.persist();
    setState(event.target.value);
  };

  const handlerSubmitFind = (event) => {
    event.preventDefault();
    if (state.trim()) {
      resultNews(filterNews(state.trim()));
    }
  };

  return (
    <form className="find-form" onSubmit={handlerSubmitFind}>
      <Select changeFilter={changeFilter} options={options} />
      <Input className="form-control" type="text" onChange={changeInputHandler} />
      <Button className="btn btn-info" type="submit" title="Find" />
    </form>
  );
};

FindNews.propTypes = {
  news: PropTypes.arrayOf(PropTypes.objectOf),
  filter: PropTypes.arrayOf(PropTypes.objectOf).isRequired,
};

FindNews.defaultProps = {
  news: [],
};

const mapDispatchToProps = {
  findAll,
  findTags,
  findAuthors,
  putNews,
  resultNews,
};

const mapStateToProps = (state) => ({
  news: state.news.value,
  filter: state.find.filter,
});

export default connect(mapStateToProps, mapDispatchToProps)(FindNews);
