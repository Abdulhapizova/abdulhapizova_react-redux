import React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import PropTypes, { objectOf } from 'prop-types';
import { findNewsUser } from '../redux/actions';
import ListItem from './ListItems';

const getUserFromLocalStorage = () => JSON.parse(localStorage.getItem('user')) || [];
const PAGE_SIZE = 5;

const NewsList = ({ news, pagination, filterNews, filterArray }) => {
  const nameUser = getUserFromLocalStorage().users_name;
  const nameId = getUserFromLocalStorage().id;

  let newArray = [];
  let renderArray;

  if (!news || !renderArray) {
    findNewsUser(nameUser, nameId);
    return <p> Нет новостей для показа </p>;
  }
  if (news.length !== 0) {
    newArray = _.chunk(news, PAGE_SIZE);
    renderArray = newArray[0];
  } else if (filterNews.length) {
    newArray = _.chunk(filterNews, PAGE_SIZE);
    renderArray = newArray[0];
  }

  if (filterArray.length !== 0) {
    renderArray = filterArray;
  }

  if (pagination.length) {
    renderArray = pagination;
  }

  return (
    <>
      <p className="title center">Список новостей</p>
      <ul className="card list">
        {renderArray.map((newsList) => (
          <ListItem
            key={newsList.id}
            title={newsList.news_title}
            author={newsList.news_author}
            description={newsList.news_description}
            tag={newsList.news_tag}
            picture={newsList.news_picture}
          />
        ))}
      </ul>
    </>
  );
};

const mapStateToProps = (state) => ({
  news: state.news.value,
  filterNews: state.filterNews.value,
  pagination: state.pagination.value,
});

const mapDispatchToProps = {
  findNewsUser,
};

NewsList.propTypes = {
  news: PropTypes.arrayOf(objectOf).isRequired,
  pagination: PropTypes.arrayOf(objectOf),
  filterNews: PropTypes.arrayOf(objectOf).isRequired,
  filterArray: PropTypes.arrayOf(objectOf).isRequired,
};

NewsList.defaultProps = {
  pagination: [],
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);
