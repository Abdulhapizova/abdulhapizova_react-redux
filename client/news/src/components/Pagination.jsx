import React, { memo } from 'react';
import ReactPaginate from 'react-paginate';
import PropTypes from 'prop-types';

const Pagination = ({ page, pageChangeHandler }) => (
  <ReactPaginate
    previousLabel="<"
    nextLabel=">"
    breakLabel="..."
    breakClassName="break-me"
    pageCount={page}
    marginPagesDisplayed={2}
    pageRangeDisplayed={5}
    onPageChange={pageChangeHandler}
    containerClassName="pagination"
    activeClassName="active"
    pageClassName="page-item"
    pageLinkClassName="page-link"
    previousClassName="page-item"
    nextClassName="page-item"
    previousLinkClassName="page-link"
    nextLinkClassName="page-link"
  />
);

Pagination.propTypes = {
  page: PropTypes.number.isRequired,
  pageChangeHandler: PropTypes.func.isRequired,
};

export default memo(Pagination);
