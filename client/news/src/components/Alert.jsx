import React from 'react';
import PropTypes from 'prop-types';

const Alert = ({ text }) => {
  return <div data-testid="text-display">{text}</div>;
};

Alert.defaultProps = {
  text: '',
};

Alert.propTypes = {
  text: PropTypes.string,
};

export default Alert;