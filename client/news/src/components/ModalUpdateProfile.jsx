import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const FormDialogUpdate = ({
  handleChangeInputLogin,
  handleChangeInputPassword,
  handleChangeInputAvatar,
  handleSubmit,
}) => {
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  return (
    <>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Редактировать профиль
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Редактировать</DialogTitle>
        <DialogContent>
          <DialogContentText>Тут Вы можете изменить Ваши персональные данные</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Новый логин"
            type="text"
            onChange={handleChangeInputLogin}
          />
          <TextField
            margin="dense"
            id="password"
            label="Новый пароль"
            type="password"
            onChange={handleChangeInputPassword}
          />
          <TextField margin="dense" id="avatar" type="file" accept="image/*" onChange={handleChangeInputAvatar} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Закрыть
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Обновить данные
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

FormDialogUpdate.propTypes = {
  handleChangeInputLogin: PropTypes.func.isRequired,
  handleChangeInputPassword: PropTypes.func.isRequired,
  handleChangeInputAvatar: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default memo(FormDialogUpdate);
