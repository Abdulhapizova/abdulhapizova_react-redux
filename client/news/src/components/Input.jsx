import React, { memo } from 'react';
import PropTypes from 'prop-types';

const Input = ({ className, type, onChange, placeholder }) => {
  return <input className={className} type={type} id="input-test" onChange={onChange} placeholder={placeholder} />;
};

Input.propTypes = {
  className: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
};

export default memo(Input);
