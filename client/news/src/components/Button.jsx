import React, { memo } from 'react';
import PropTypes from 'prop-types';

const COMPONENTS = ['button', 'a'];
const TYPES = ['button', 'submit', 'reset'];

const Button = ({ component = 'button', className, type = 'button', title, onClick = () => {}, ...rest }) => {
  const Component = component;
  const isButton = component === 'button';

  return (
    <Component type={isButton && type} className={className} onClick={onClick} {...rest}>
      {title}
    </Component>
  );
};

Button.propTypes = {
  component: PropTypes.oneOf(COMPONENTS),
  type: PropTypes.oneOf(TYPES),
  className: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

export default memo(Button);
