import React from 'react';
import PropTypes from 'prop-types';

const ItemsList = ({ key, title, author, description, tag, picture }) => {
  return [
    <li className="list-item" key={key}>
      <p>
        <img
          className="image"
          src={`http://f59e6d1a36c3.ngrok.io/uploads${picture}`}
          alt="тут должна быть ваша картинка"
        />
      </p>
      <p className="card-text">
        Заголовок: &nbsp;
        {title}
      </p>
      <p className="card-text">
        автор: &nbsp;
        {author}
      </p>
      <p className="card-text">
        <img
          className="image"
          src={`http://f59e6d1a36c3.ngrok.io/uploads/profile/${author}`}
          alt="тут должна быть аватарка"
        />
      </p>
      <p className="card-text">
        Содержимое: <br />
        {description}
      </p>
      <p className="card-text">
        теги: &nbsp;
        {tag}
      </p>
    </li>,
  ];
};

ItemsList.propTypes = {
  key: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  tag: PropTypes.string.isRequired,
  picture: PropTypes.string.isRequired,
};

export default ItemsList;
