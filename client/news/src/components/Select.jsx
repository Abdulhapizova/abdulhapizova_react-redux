import React, { memo } from 'react';
import PropTypes from 'prop-types';

const Select = ({ changeFilter, options }) => {
  return (
    <select className="form-control" name="select" onChange={changeFilter} data-testid="select">
      {options.map((item) => (
        <option key={item.title} value={item.title} data-testid="select-option">
          {' '}
          {item.title}{' '}
        </option>
      ))}
    </select>
  );
};

Select.propTypes = {
  changeFilter: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default memo(Select);
