import React from 'react';
import * as _ from 'lodash';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { findOtherUser, paginationNews, pagination } from '../redux/actions';
import ListItems from './ListItems';
import Pagination from './Pagination';

const PAGE_SIZE = 5;

const CabinetOtherUser = ({ news = [], name, pagination, paginationNews, paginationArray, filterNews }) => {
  let newArray = [];
  let renderArray = [];
  let pages;

  if (!filterNews.length) {
    newArray = _.chunk(news, PAGE_SIZE);
    [renderArray] = newArray;
  } else {
    newArray = _.chunk(filterNews, PAGE_SIZE);
    [renderArray] = newArray;
  }

  const pageChangeHandler = (selectPage) => {
    pagination(selectPage.selected);
    paginationNews(newArray[selectPage.selected]);
  };

  if (!news.length || !renderArray) {
    findOtherUser(name);
  } else if (news.length !== 0) {
    newArray = _.chunk(news, PAGE_SIZE);
    [renderArray] = newArray;
  } else {
    pages = 0;
  }

  if (paginationArray.length) {
    renderArray = paginationArray;
  }

  return (
    <div className="container">
      {!news.length || !renderArray ? (
        <p> Нет новостей для показа </p>
      ) : (
        <>
          <div>
            <p>Новости пользователя:</p>
            <Link to="/">Назад</Link>
          </div>
          <ul className="card list">
            {renderArray.map((newsList) => (
              <ListItems
                key={newsList.id}
                title={newsList.news_title}
                author={newsList.news_author}
                description={newsList.news_description}
                tag={newsList.news_tag}
                picture={newsList.news_picture}
              />
            ))}
          </ul>
          <Pagination className="pagination" pageChangeHandler={pageChangeHandler} page={pages} />
        </>
      )}
    </div>
  );
};

CabinetOtherUser.propTypes = {
  news: PropTypes.arrayOf(PropTypes.objectOf()).isRequired,
  name: PropTypes.arrayOf(PropTypes.objectOf()).isRequired,
  paginationArray: PropTypes.arrayOf(PropTypes.objectOf()),
  filterNews: PropTypes.arrayOf(PropTypes.objectOf()),
  pagination: PropTypes.func.isRequired,
  paginationNews: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  news: state.findOther.value,
  paginationArray: state.pagination.value,
  filterNews: state.filterNews.value,
});

const mapDispatchToProps = {
  findOtherUser,
  pagination,
  paginationNews,
};

export default connect(mapStateToProps, mapDispatchToProps)(CabinetOtherUser);
