import React, { memo, useState } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '../components/Button';
import { findOtherUser } from '../redux/actions';

const FormFindNewsUser = ({ findOtherUser }) => {
  const [user, setUser] = useState('');

  const changeFindHandler = (event) => {
    setUser(event.target.value);
  };

  const handlerFindUser = (event) => {
    event.preventDefault();
    findOtherUser(user);
  };

  const handlerSubmitFind = () => {
    findOtherUser(user);
  };
  return (
    <form className="form" onSubmit={handlerFindUser}>
      Найти новости пользователя:
      <input className="form-control position" type="text" onChange={changeFindHandler} />
      <Link to={`/enter/${user}`}>
        <Button className="btn btn-primary mb-2" type="button" onClick={handlerSubmitFind} title="Найти" />
      </Link>
    </form>
  );
};

const mapDispatchToProps = {
  findOtherUser,
};

FormFindNewsUser.propTypes = {
  findOtherUser: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(memo(FormFindNewsUser));
