import React, { memo, useState, useRef, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { connect } from 'react-redux';
import { addNewFromUser } from '../redux/actions';

const INITIAL_STATE = {
  title: '',
  description: '',
  tag: '',
  author: '',
};

const ModalForm = ({ author, addNewFromUser }) => {
  const [open, setOpen] = useState(false);
  const [newNews, setNewNews] = useState(INITIAL_STATE);
  const pictureRef = useRef(null);

  useEffect(() => {
    setNewNews((prev) => ({ ...prev, author }));
  }, [author]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChangeInputTitle = (event) => {
    if (event.target.value.trim()) {
      setNewNews((prev) => ({ ...prev, title: event.target.value.trim() }));
    }
  };

  const handleChangeInputDescription = (event) => {
    if (event.target.value.trim()) {
      setNewNews((prev) => ({ ...prev, description: event.target.value.trim() }));
    }
  };

  const handleChangeInputTag = (event) => {
    if (event.target.value.trim()) {
      setNewNews((prev) => ({ ...prev, tag: event.target.value.trim() }));
    }
  };

  const handleChangeInputFile = (event) => {
    [pictureRef.current] = event.target.files;
  };

  const handleSubmitForm = () => {
    if (newNews.title && newNews.author && newNews.description && newNews.tag) {
      addNewFromUser(newNews.title, newNews.author, newNews.description, newNews.tag, pictureRef.current);
      setOpen(false);
      setNewNews(INITIAL_STATE);
    }
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Добавить новость
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Добавление новости</DialogTitle>
        <DialogContent>
          <DialogContentText>Что бы добавить новую запись, пожалуйста, заполните все поля ниже:</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="title"
            label="Заголовок"
            type="text"
            onChange={handleChangeInputTitle}
          />
          <TextField
            margin="dense"
            id="description"
            label="Описание"
            type="text"
            fullWidth
            onChange={handleChangeInputDescription}
          />
          <TextField margin="dense" id="tag" label="Тэги" type="text" onChange={handleChangeInputTag} />
          <TextField
            margin="dense"
            id="picture"
            label="Изображение"
            type="file"
            accept="image/*"
            onChange={handleChangeInputFile}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Закрыть
          </Button>
          <Button onClick={handleSubmitForm} color="primary">
            Добавить запись
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

ModalForm.propTypes = {
  author: PropTypes.string.isRequired,
  addNewFromUser: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  addNewFromUser,
};

export default connect(null, mapDispatchToProps)(memo(ModalForm));
