import {
  FIND_ALL_NEWS,
  FIND_TAGS_NEWS,
  FIND_AUTHORS_NEWS,
  FILTER_NEWS,
  PAGINATION_NEWS,
  CHANGE_PAGE,
  REQUEST_POSTS,
  ADD_USERS,
  SUCCESS_USERS,
  SHOW_ALERT,
  ADD_TITLE,
  ADD_DESCRIPTION,
  ADD_TAG,
  ADD_NEWS,
  SUCCESS_ADD_NEWS,
  FIND_USER,
  SUCCESS_FIND_USERS,
  FIND_NEWS_USER,
  SUCCESSFUL_NEWS_USER,
  ERROR_FIND,
  UPDATE_DATA,
  FIND_OTHER_USER,
  SUCCESS_FIND_OTHER,
  PUT_NEWS,
  SUCCESS_UPDATE_DATA,
} from './types';

export function putNews(news) {
  return {
    type: REQUEST_POSTS,
    payload: news,
  };
}

export function requestNews(news) {
  return {
    type: PUT_NEWS,
    payload: news,
  };
}

export function resultNews(result) {
  return {
    type: FILTER_NEWS,
    payload: result,
  };
}

export function findAll(filter) {
  return {
    type: FIND_ALL_NEWS,
    payload: filter,
  };
}

export function findTags(filter) {
  return {
    type: FIND_TAGS_NEWS,
    payload: filter,
  };
}

export function findAuthors(filter) {
  return {
    type: FIND_AUTHORS_NEWS,
    payload: filter,
  };
}

export function pagination(page) {
  return {
    type: CHANGE_PAGE,
    payload: page,
  };
}

export function paginationNews(news) {
  return {
    type: PAGINATION_NEWS,
    payload: news,
  };
}

export function addNewUser(data, avatar) {
  return {
    type: ADD_USERS,
    payload: {
      data,
      avatar,
    },
  };
}

export function successAddUser(inform) {
  return {
    type: SUCCESS_USERS,
    payload: inform,
  };
}

export function showAlert(text) {
  return {
    type: SHOW_ALERT,
    payload: text,
  };
}

export function addTitle(title) {
  return {
    type: ADD_TITLE,
    payload: title,
  };
}

export function addDescription(description) {
  return {
    type: ADD_DESCRIPTION,
    payload: description,
  };
}

export function addTag(tag) {
  return {
    type: ADD_TAG,
    payload: tag,
  };
}

export function addNewFromUser(title, author, description, tag, picture) {
  return {
    type: ADD_NEWS,
    payload: {
      title,
      author,
      description,
      tag,
      picture,
    },
  };
}

export function successAddNews(news) {
  return {
    type: SUCCESS_ADD_NEWS,
    payload: news,
  };
}

export function findOldUser(password, name) {
  return {
    type: FIND_USER,
    payload: {
      password,
      name,
    },
  };
}

export function successFindUser(data) {
  return {
    type: SUCCESS_FIND_USERS,
    payload: {
      id: data.id,
      name: data.name,
    },
  };
}

export function errorFindUser() {
  return {
    type: ERROR_FIND,
  };
}

export function findNewsUser(name, id) {
  return {
    type: FIND_NEWS_USER,
    payload: {
      name,
      id,
    },
  };
}

export function successfulFindNews(news) {
  return {
    type: SUCCESSFUL_NEWS_USER,
    payload: news,
  };
}

export function successUpdate(data) {
  return {
    type: SUCCESS_UPDATE_DATA,
    payload: data,
  };
}

export function updatePersonalData(id, login, password, avatar) {
  return {
    type: UPDATE_DATA,
    payload: {
      id,
      login,
      password,
      avatar,
    },
  };
}

export function findOtherUser(name) {
  return {
    type: FIND_OTHER_USER,
    payload: name,
  };
}

export function successFindOther(news) {
  return {
    type: SUCCESS_FIND_OTHER,
    payload: news,
  };
}
