import { combineReducers } from 'redux';
import PutReducer from './PutReducer';
import FindReducer from './FindReducer';
import PaginationReducer from './PaginationReducer';
import FilterReducer from './FilterReducer';
import PageReducer from './PageReducer';
import AddUserReducer from './AddUserReducer';
import AddNewsReducer from './AddNewsReducer';
import FindUserReducer from './FindUserReducer';
import findOtherReducer from './FindOtherReducer';

const rootReducer = combineReducers({
  news: PutReducer,
  find: FindReducer,
  filterNews: FilterReducer,
  page: PageReducer,
  pagination: PaginationReducer,
  addUser: AddUserReducer,
  addNewsFromUser: AddNewsReducer,
  findUser: FindUserReducer,
  findOther: findOtherReducer,
});

export default rootReducer;
