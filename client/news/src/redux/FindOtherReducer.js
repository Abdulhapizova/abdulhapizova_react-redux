import { FIND_OTHER_USER, SUCCESS_FIND_OTHER } from './types';

const initialState = {
  name: '',
};

function findOtherReducer(state = initialState, action) {
  switch (action.type) {
    case FIND_OTHER_USER:
      return { ...state, name: action.payload };
    case SUCCESS_FIND_OTHER:
      return { ...state, value: action.payload };
    default:
      return state;
  }
}

export default findOtherReducer;
