import { FILTER_NEWS } from './types';

const initialState = {
  filterNews: [],
};

function filterReducer(state = initialState, action) {
  switch (action.type) {
    case FILTER_NEWS:
      return { ...state, value: action.payload };
    default:
      return state;
  }
}

export default filterReducer;
