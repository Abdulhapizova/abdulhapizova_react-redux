import { ADD_NEWS } from './types';

const initialState = {
  title: '',
  author: '',
  description: '',
  tag: '',
};

function addNews(state = initialState, action) {
  switch (action.type) {
    case ADD_NEWS:
      return {
        ...state,
        title: action.payload.title,
        author: action.payload.author,
        description: action.payload.description,
        tag: action.payload.tag,
        picture: action.payload.picture,
      };
    default:
      return state;
  }
}

export default addNews;
