import { FIND_USER, SUCCESS_FIND_USERS, FIND_NEWS_USER, SHOW_ALERT } from './types';

const initialState = {
  user_password: '',
  user_name: '',
  user_id: null,
  token: '',
  alert: '',
  id: '',
  name: '',
};

function findUserReducer(state = initialState, action) {
  switch (action.type) {
    case FIND_USER:
      return { ...state, user_password: action.payload.password, user_name: action.payload.name };
    case SUCCESS_FIND_USERS:
      return {
        ...state,
        user_name: action.value.name,
        user_id: action.value.id,
        token: action.value.token,
      };
    case SHOW_ALERT:
      return { alert: 'ERROR OF FIND' };
    case FIND_NEWS_USER:
      return { ...state, id: action.id, name: action.name };
    default:
      return state;
  }
}

export default findUserReducer;
