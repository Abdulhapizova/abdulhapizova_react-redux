import { FIND_ALL_NEWS, FIND_TAGS_NEWS, FIND_AUTHORS_NEWS } from './types';

const initialState = {
  filter: ['all'],
};

function findReducer(state = initialState, action) {
  switch (action.type) {
    case FIND_ALL_NEWS:
      return { ...state, filter: action.payload };
    case FIND_TAGS_NEWS:
      return { ...state, filter: action.payload };
    case FIND_AUTHORS_NEWS:
      return { ...state, filter: action.payload };
    default:
      return state;
  }
}

export default findReducer;
