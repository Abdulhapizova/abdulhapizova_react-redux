import { CHANGE_PAGE } from './types';

const initialState = {
  page: 0,
};

function pageReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_PAGE:
      return { value: action.value };
    default:
      return state;
  }
}

export default pageReducer;
