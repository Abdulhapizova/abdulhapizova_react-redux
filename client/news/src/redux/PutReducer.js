import { PUT_NEWS, SUCCESSFUL_NEWS_USER, SUCCESS_ADD_NEWS } from './types';

const initialState = {
  news: [],
};

function putReducer(state = initialState, action) {
  switch (action.type) {
    case PUT_NEWS:
      return { value: action.payload };
    case SUCCESSFUL_NEWS_USER:
      return { value: action.payload };
    case SUCCESS_ADD_NEWS:
      return { ...state, value: [...state.value, action.payload] };
    default:
      return state;
  }
}

export default putReducer;
