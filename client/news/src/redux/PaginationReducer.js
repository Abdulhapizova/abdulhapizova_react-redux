import { PAGINATION_NEWS } from './types';

const initialState = {
  pagination: [],
};

function paginationReducer(state = initialState, action) {
  switch (action.type) {
    case PAGINATION_NEWS:
      return { value: action.payload };
    default:
      return state;
  }
}

export default paginationReducer;
