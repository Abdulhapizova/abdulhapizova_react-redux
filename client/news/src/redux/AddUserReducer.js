import { ADD_USER, SUCCESS_USERS, SHOW_ALERT, SUCCESS_UPDATE_DATA } from './types';

const initialState = {
  addUsers: [],
  alert: null,
  users: [],
};

function addReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_USER:
      return { ...state, addUsers: action.payload };
    case SHOW_ALERT:
      return { ...state, alert: action.payload };
    case SUCCESS_USERS:
      return { ...state, addUsers: action.payload };
    case SUCCESS_UPDATE_DATA:
      return { ...state, users: action.payload };
    default:
      return state;
  }
}

export default addReducer;
