import axios from 'axios'

const axiosUser = (oldUsers) => {
    return axios({
        method: "post",
        url: "http://f59e6d1a36c3.ngrok.io/users/signin/",
        data: JSON.stringify(oldUsers),
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
          },
      })
        .then(response => response.data)
        .catch(err => {
          throw err;
        });
    };

    export default axiosUser;