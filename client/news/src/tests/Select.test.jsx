import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import Select from '../components/Select';

test('Simulates selection', () => {
  const mockOnClick = jest.fn();
  const option = [{ title: 'all' }, { title: 'tags' }, { title: 'authors' }];
  const { getByTestId, getAllByTestId } = render(<Select changeFilter={mockOnClick} options={option} />);

  fireEvent.change(getByTestId('select'), { target: { value: 'tags' } });
  const options = getAllByTestId('select-option');
  expect(options[0].selected).toBeFalsy();
  expect(options[1].selected).toBeTruthy();
  expect(options[2].selected).toBeFalsy();
});
