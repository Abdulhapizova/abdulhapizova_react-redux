import React from 'react';
import { render, fireEvent } from './test-util/test-utils';
import Input from '../components/Input';

describe('input testing', () => {
  const handlerChange = jest.fn();
  beforeEach(() => {
    render(<Input onChange={handlerChange} />);
  });

  test('check the onChange callback in Input component', () => {
    const input = document.querySelector('#input-test');

    fireEvent.change(input, { target: { value: 'Victoria' } });
    expect(input.value).toBe('Victoria');
  });
});
