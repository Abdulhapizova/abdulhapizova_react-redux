import React from 'react';
import { screen, renderWithRouter } from './test-util/test-utils';
import App from '../App';

test('landing on a bad page', () => {
  renderWithRouter(<App />, { route: '/something-that-does-not-match' });

  expect(screen.getByText('404 - Not Found!')).toBeInTheDocument();
});
