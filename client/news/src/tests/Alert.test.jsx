import React from 'react';
import { render, screen } from '@testing-library/react';
import Alert from '../components/Alert';

test('render Alert component with and without props', () => {
  const { rerender } = render(<Alert text="Alert" />);
  expect(screen.getByTestId('text-display')).toHaveTextContent('Alert');

  rerender(<Alert />);
  expect(screen.getByTestId('text-display')).toHaveTextContent('');
});
