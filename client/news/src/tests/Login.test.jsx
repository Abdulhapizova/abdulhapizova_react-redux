import React from 'react';
import configureStore from 'redux-mock-store';
import { render, fireEvent } from './test-util/test-utils';
import { findOldUser } from '../redux/actions';

import Login from '../components/Login';

const middlewares = [];
const mockStore = configureStore(middlewares);

describe('test for component Login', () => {
  const user = {
    login: 'Victoria',
    password: 'password',
  };

  let store;

  beforeEach(() => {
    const initialState = {};
    store = mockStore(initialState);
    store.dispatch = jest.fn();
    render(<Login store={store} />);
  });

  test('login works right', () => {
    const [login, password] = document.querySelectorAll('.form-control');
    const form = document.querySelector('#accept-form');

    const action = findOldUser(user.login, user.password);

    fireEvent.change(login, { target: { value: user.login } });
    fireEvent.change(password, { target: { value: user.password } });
    fireEvent.submit(form);

    expect(store.dispatch).toHaveBeenCalled();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});
