import React from 'react'
import {
  BrowserRouter,
} from "react-router-dom";
import { render as rndRedux} from '@testing-library/react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'

import userReducer from '../../redux/rootReducer'


function render(
  ui,
  {
    preloadedState,
    store = createStore(userReducer),
    ...renderOptions
  } = {}
) {
  function Wrapper({ children }) {
    return <Provider store={store}>{children}</Provider>
  }
  return rndRedux(ui, { wrapper: Wrapper, ...renderOptions })
}

const renderWithRouter = (ui, {route = '/'} = {}) => {
  window.history.pushState({}, 'Test page', route)

  return render(ui, {wrapper: BrowserRouter})
}

// re-export everything
export * from '@testing-library/react'
// override render method
export { render, renderWithRouter }