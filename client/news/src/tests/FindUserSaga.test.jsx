import { testSaga } from 'redux-saga-test-plan';
import mockAxios from 'jest-mock-axios';
import { findUser } from '../saga/findUser';
import { successFindUser } from '../redux/actions';
import axiosUsers from '../../utils/axios';

describe('test for component Login', () => {
  afterEach(() => {
    mockAxios.reset();
  });

  const action = {
    payload: {
      password: 'Victoria',
      name: 'password',
    },
  };

  const user = {
    users_password: action.payload.password,
    users_name: action.payload.name,
  };

  test('FIND_USER correct works', async () => {
    testSaga(findUser, action)
      .next()
      .call(axiosUsers, user)
      .next({ id: 0, name: 'Victoria' })
      .put(successFindUser({ id: 0, name: 'Victoria' }))
      .finish()
      .isDone();
  });
});
