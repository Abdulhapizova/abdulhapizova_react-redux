const { check } = require('../controllers/checkUsers');

describe('test check user', () => {
  let res;
  let sendMock;
  let statusMock;

  beforeEach(() => {
    res = {};
    sendMock = jest.fn(({ message }) => message);
    statusMock = jest.fn(() => {
      return {
        send: sendMock,
      };
    });
    res.status = statusMock;
  });

  test('should throw 401 error if name is empty string', async () => {
    const req = { body: { users_name: '' } };

    await check(req, res);
    expect(statusMock).toHaveBeenCalledWith(401);
    expect(sendMock).toHaveBeenCalledWith({ message: 'Authentication failed. User not found.' });
  });

  test('should throw 500 error if password wrong', async () => {
    const req = { body: { users_name: 'Viktoria', users_password: 'word' } };

    await check(req, res);
    expect(statusMock).toHaveBeenCalledWith(500);
    expect(sendMock).toHaveBeenCalledWith({ message: 'Authentication failed. Wrong password.' });
  });

  test('should correct if password right', async () => {
    const req = { body: { users_name: 'Viktoria', users_password: 'password' } };

    await check(req, res);
    expect(statusMock).toHaveBeenCalledWith(200);
    expect(sendMock).toHaveBeenCalledWith({ message: 'Right password' });
  });
});
