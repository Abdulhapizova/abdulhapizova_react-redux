const request = require('supertest');
const app = require('../app');

jest.mock('../query/news.query', () => {
  return {
    getNews: () => {
      return new Promise((res) => {
        res({
          news_title: 'title',
          news_author: 'author',
          news_description: 'description',
          news_tag: 'tag',
          token: 'token',
          news_picture: 'picture',
        });
      });
    },
  };
});

describe('GET /news', () => {
  test('test defined response body', async () => {
    const response = await request(app).get('/news');
    expect(response.body.news_title).toBeDefined();
    expect(response.body.news_author).toBeDefined();
    expect(response.body.news_description).toBeDefined();
    expect(response.body.news_tag).toBeDefined();
    expect(response.body.news_picture).toBeDefined();
  });

  test('test bad request', async () => {
    const response = await request(app).get('/new');
    expect(response.statusCode).toEqual(404);
  });
});
