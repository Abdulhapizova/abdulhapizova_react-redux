const validation = require('../utills/validation');

describe('Validation middleware', () => {
  const requestWithoutObj = {};
  const requestWithObj = {
    body: {
      users_name: 'viktoria@gmail.com',
      users_password: 'password',
      token: 'token',
    },
  };
  const mockResponse = {
    json: jest.fn(),
  };
  const nextFunction = jest.fn();

  test('test without params', async () => {
    const expectedResponse = {
      error: 'Missing params',
    };
    validation(requestWithoutObj, mockResponse, nextFunction);

    expect(mockResponse.json).toBeCalledWith(expectedResponse);
  });

  test('test with params', async () => {
    validation(requestWithObj, mockResponse, nextFunction);

    expect(nextFunction).toBeCalledTimes(1);
  });
});
