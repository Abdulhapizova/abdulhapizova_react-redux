const request = require('supertest');
const app = require('../app');

jest.mock('../query/users.query.js', () => {
  return {
    getUserbyId: () => {
      return new Promise((res) => {
        res({
          login: 'vicktoria@gmail.com',
          password: 'password',
          avatar: 'avatar',
        });
      });
    },
  };
});

describe('GET /users', () => {
  test('test to get user with status 200', async () => {
    const response = await request(app).get('/users/1');
    expect(response.statusCode).toEqual(200);
  });
  test('test defined response', async () => {
    const response = await request(app).get('/users/1');
    expect(response.body.login).toBeDefined();
    expect(response.body.password).toBeDefined();
    expect(response.body.avatar).toBeDefined();
  });
});
