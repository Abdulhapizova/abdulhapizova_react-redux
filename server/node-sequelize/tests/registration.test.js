const request = require('supertest');
const app = require('../app');

const requestObj = {
  users_name: 'viktoria@gmail.com',
  users_password: 'password',
  token: 'token',
};

const requestObjWithoutPassword = {
  users_name: 'viktoria@gmail.com',
  token: 'token',
};

jest.mock('../query/users.query', () => {
  return {
    getUserbyLogin: () => {
      return new Promise((res) => {
        res(null);
      });
    },
    add: () => {
      return new Promise((res) => {
        res({
          users_name: 'name',
          users_password: 'password',
          users_avatar: 'avatar',
          users_token: 'token',
        });
      });
    },
  };
});

describe('POST /users', () => {
  test('test add new user', async () => {
    const response = await request(app).post('/users/signup').send(requestObj);
    expect(response.statusCode).toEqual(200);
  });

  test('test add new user without password', async () => {
    const response = await request(app).post('/users/signup').send(requestObjWithoutPassword);
    expect(response.statusCode).toEqual(400);
  });
});
