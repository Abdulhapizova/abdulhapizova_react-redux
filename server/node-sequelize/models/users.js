const {
  Model,
} = require('sequelize');
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
  }
  Users.init({
    users_name: DataTypes.STRING,
    users_password: DataTypes.STRING,
    users_avatar: DataTypes.STRING,
    users_token: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Users',
  });

  Users.beforeSave((user) => {
    if (user.changed('users_password')) {
      user.users_password = bcrypt.hashSync(user.users_password, bcrypt.genSaltSync(10));
    }
  });

  Users.beforeBulkUpdate((user) => {
    user.attributes.users_password = bcrypt.hashSync(user.attributes.users_password, bcrypt.genSaltSync(10));
  });

  Users.prototype.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.users_password, function (err, isMatch) {
      if (err) {
        return cb(err);
      }
      cb(null, isMatch);
    });
  };
  return Users;
};
