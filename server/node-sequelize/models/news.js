const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class News extends Model {
  }
  News.init({
    news_title: DataTypes.STRING,
    news_author: DataTypes.STRING,
    news_description: DataTypes.STRING,
    news_tag: DataTypes.STRING,
    news_picture: DataTypes.STRING,
    token: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'News',
  });
  return News;
};
