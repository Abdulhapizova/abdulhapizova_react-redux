const jwt = require('jsonwebtoken');
const path = require('path');
// eslint-disable-next-line prefer-destructuring
const Users = require('../models').Users;
const { getUserbyLogin, add, getUserbyId } = require('../query/users.query');

module.exports = {
  async getUser(req, res) {
    try {
      const { id } = req.params;
      const user = await getUserbyId({ id });
      return res.status(200).send(user);
    } catch (error) {
      return res.status(500).send(error);
    }
  },

  async add(req, res) {
    try {
      if (!req.body.users_password) {
        return res.status(400).send('Please pass username and password.');
      }
      const users = await getUserbyLogin(req.body.users_name);
      if (!users) {
        const token = jwt.sign(JSON.parse(JSON.stringify(req.body)), 'nodeauthsecret', { expiresIn: 86400 * 30 });
        const user = await add({
          users_name: req.body.users_name,
          users_password: req.body.users_password,
          users_token: token,
        });
        return res.status(200).send(user);
      }
    } catch (error) {
      return res.status(500).send(error);
    }
  },

  async photo(req, res) {
    try {
      const users = await Users.findOne({
        where: {
          users_name: req.params.id,
        },
      });
      return res.sendFile(path.resolve(`./uploads/profile/${users.users_avatar}`));
    } catch (error) {
      return res.status(500).send(error);
    }
  },

  update(req, res) {
    let filename;
    if (req.body.users_avatar) {
      filename = `${req.body.users_avatar}`;
    } else {
      filename = `${req.file.filename}`;
    }
    return Users.update(
      {
        users_name: req.body.users_name,
        users_password: req.body.users_password,
        users_avatar: filename,
      },
      {
        where: {
          id: req.body.id,
        },
      },
    )
      .then(() =>
        Users.findOne({
          where: {
            id: req.body.id,
          },
        }),
      )
      .then((users) => res.send(users).sendStatus(200))
      .catch((err) => res.status(500).send(err));
  },
};
