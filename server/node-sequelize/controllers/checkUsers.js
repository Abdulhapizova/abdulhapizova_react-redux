const { getUserbyLogin } = require('../query/check.query');

const check = async (req, res) => {
  try {
    const users = await getUserbyLogin(req.body.users_name);
    if (!users.get('user_name')) {
      return res.status(401).send({
        message: 'Authentication failed. User not found.',
      });
    }
    const hash = await users.hash(users.get('users_password'));
    const checkPass = await users.comparePassword({ new: req.body.users_password, origin: hash });

    if (!checkPass) {
      return res.status(500).send({
        message: 'Authentication failed. Wrong password.',
      });
    }
    return res.status(200).send({
      message: 'Right password',
    });
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = {
  check,
};
