const path = require('path');
// eslint-disable-next-line prefer-destructuring
const News = require('../models').News;
const { getNews } = require('../query/news.query');

module.exports = {
  async list(_, res) {
    try {
      const news = await getNews();
      return res.status(200).send(news);
    } catch (error) {
      return res.status(500).send(error);
    }
  },

  async photo(req, res) {
    try {
      const news = await News.findAll({
        where: {
          news_picture: req.url,
        },
      });
      return news.sendFile(path.resolve(`./uploads/photo/${req.url}`));
    } catch (error) {
      return res.status(500).send(error);
    }
  },

  async find(req, res) {
    try {
      const news = await News.findAll({
        where: {
          token: req.body.token,
        },
      });
      return res.send(news).status(200);
    } catch (error) {
      return res.status(500).send(error);
    }
  },

  async findOther(req, res) {
    try {
      const news = await News.findAll({
        where: {
          news_author: req.body.name,
        },
      });
      return res.send(news).status(200);
    } catch (error) {
      return res.status(500).send(error);
    }
  },

  async add(req, res) {
    try {
      const filename = `/${req.file.filename}`;
      const news = await News.create({
        news_title: req.body.title,
        news_author: req.body.author,
        news_description: req.body.description,
        news_tag: req.body.tag,
        token: req.body.token,
        news_picture: filename,
      });
      return res.status(201).send(news);
    } catch (error) {
      return res.status(500).send(error);
    }
  },
};
