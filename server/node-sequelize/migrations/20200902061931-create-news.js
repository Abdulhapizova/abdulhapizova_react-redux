module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('News', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      news_id: {
        type: Sequelize.INTEGER,
      },
      news_title: {
        type: Sequelize.STRING,
      },
      news_author: {
        type: Sequelize.STRING,
      },
      news_description: {
        type: Sequelize.STRING,
      },
      news_tag: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable('News');
  },
};
