var SequelizeMock = require('sequelize-mock');
var DBConnectionMock = new SequelizeMock();
const bcrypt = require('bcrypt');

var UserMock = DBConnectionMock.define('users', {
    'users_name': 'Viktoria',
    'users_password': 'password',
    'users_avatar': 'user-picture.jpg',
    'users_token': 'user-picture.jpg',
}, {
    instanceMethods: {
        hash: async function(password) {
            const salt = await bcrypt.genSalt(10);
            return await bcrypt.hash(password, salt);
        },
        comparePassword: async function (password) {
            try{
            return await bcrypt.compare(password.new, password.origin) 
        } catch(err) {
            console.log('err', err)
        }
        },
    },
});

module.exports = {
  getUserbyLogin(name) {
    return UserMock.findOne({ where: { user_name: name } });
  },
};