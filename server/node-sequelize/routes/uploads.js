const express = require('express');

const router = express.Router();
const newsControllers = require('../controllers/news');
const usersControllers = require('../controllers/users');

router.get('/:id', newsControllers.photo);
router.get('/profile/:id', usersControllers.photo);

module.exports = router;
