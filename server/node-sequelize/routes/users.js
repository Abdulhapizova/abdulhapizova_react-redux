const express = require('express');
const multer = require('multer');

const router = express.Router();
const usersControllers = require('../controllers/users');
const validator = require('../utills/validation');

const destinationAvatar = multer({ dest: 'uploads/profile' });


router.get('/:id', usersControllers.getUser);
router.post('/signup', destinationAvatar.single('users_avatar'), usersControllers.add);
router.post('/update', destinationAvatar.single('users_avatar'), usersControllers.update);
router.post('/signin', validator, usersControllers.check);

module.exports = router;
