const express = require('express');

const router = express.Router();
const register = require('../controllers/checkUsers')

router.post('/', register.registration);

module.exports = router;