const express = require('express');
const multer = require('multer');

const router = express.Router();
const newsControllers = require('../controllers/news');

const destinationImage = multer({ dest: 'uploads/photo' });

router.get('/', newsControllers.list);
router.post('/', newsControllers.find);
router.post('/:id', newsControllers.findOther);
router.post('/uploads/photo', destinationImage.single('picture'), newsControllers.add);

module.exports = router;
