const validateEmpty = (req, res, next) => {
    if (!req.body) {
      return res.json({ error: 'Missing params' });
    }
    next();
  };
  
  module.exports = validateEmpty;
  